package TC_Runner;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import Utilities.GetTCDetailsFromDataSheet;
import Utilities.Reporting_Utilities;
import Utilities.getCurrentDate;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Runner_MDMP_155_Stock_Update_Flow_from_RMS_to_BlueYounder {

	@Test
	public void test_MDMP_155_Stock_Update_Flow_from_RMS_to_BlueYounder() throws IOException {

		ExtentReports extent=null;
		ExtentTest logger=null;
		String ResultPath=null;
		XWPFRun xwpfRun=null;
		XWPFDocument doc=null;
		
		String Final_Result="FAIL";
		
		
		String  TestKeyword = "Stocks_Flow_from_RMS_to_BY";

		String TestCaseNo=GetTCDetailsFromDataSheet.TCDetails(TestKeyword).get("TestCaseNo");
		String TestCaseName=GetTCDetailsFromDataSheet.TCDetails(TestKeyword).get("TestCaseName");


		if(ResultPath==null){

			String ResPath=System.getProperty("user.dir") +"/Results"+"/"+TestCaseNo+"_"+getCurrentDate.getISTDateddMM();

			ResultPath=Reporting_Utilities.createResultPath(ResPath);	
		
			ResultPath.replace("\\", "/");
			System.setProperty("resultpath",ResultPath);
		}

		System.out.println("Result path is "+ResultPath);


		String TC_ScFolder=ResultPath+TestCaseNo;

		System.out.println("Sc result path is "+TC_ScFolder);


		File dir = new File(TC_ScFolder);

		if(!dir.exists()){

			dir.mkdir();

		}

		try{

			
			doc = new XWPFDocument();
	        XWPFParagraph p = doc.createParagraph();
	        xwpfRun = p.createRun();  
			
			
			extent = new ExtentReports (ResultPath+"MDMP_155_Stock_Update_SeleniumReport.html", false);
			extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
			//		 //*******************************************************************************************************
			//		//****************************Instantiate Extent Reporting************************
			logger=extent.startTest(TestCaseNo+":"+TestCaseName);

			MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step1_ExecuteJob_NB_RMSE_BLUEYONDER_ITEM_SOH.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			
			Result ResStep1 = JUnitCore.runClasses(MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step1_ExecuteJob_NB_RMSE_BLUEYONDER_ITEM_SOH.class);

			boolean step1= ResStep1.wasSuccessful();
			
			MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step2_Stock_RMS_to_Relex_OretailPath_FileValidation.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			Result ResStep2 = JUnitCore.runClasses(MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step2_Stock_RMS_to_Relex_OretailPath_FileValidation.class);
			

			boolean step2= ResStep2.wasSuccessful();

			MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step3_ExecuteJob_NB_BLUEYONDER_STOCKS_NAS_COPY.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			Result ResStep3 = JUnitCore.runClasses(MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step3_ExecuteJob_NB_BLUEYONDER_STOCKS_NAS_COPY.class);
			

			boolean step3= ResStep3.wasSuccessful();

			
			MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step4_Stocks_RMS_to_Relex_NASPath_FileValidation.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			Result ResStep4 = JUnitCore.runClasses(MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step4_Stocks_RMS_to_Relex_NASPath_FileValidation.class);
			

			boolean step4= ResStep4.wasSuccessful();

			MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step5_Stocks_RMS_to_BY_FileContentValidation_Oretail_And_NAS.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);


			Result ResStep5 = JUnitCore.runClasses(MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step5_Stocks_RMS_to_BY_FileContentValidation_Oretail_And_NAS.class);
			

			boolean step5= ResStep5.wasSuccessful();

			MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step6_ExecuteJob_NB_BLUEYONDER_STOCKS_NAS_UPLD.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);


			Result ResStep6 = JUnitCore.runClasses(MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step6_ExecuteJob_NB_BLUEYONDER_STOCKS_NAS_UPLD.class);
		

			boolean step6= ResStep6.wasSuccessful();


			MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step7_Stocks_RMS_to_BY_Navigate_To_S3.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);


			Result ResStep7 = JUnitCore.runClasses(MDMP_155_Stock_Update_RMS_To_BY.TC_004_Step7_Stocks_RMS_to_BY_Navigate_To_S3.class);

			boolean step7= ResStep7.wasSuccessful();	
			
			
			if(step1&&step2&&step3&&step4&&step5&&step6&&step7){

				
				 Final_Result="PASS";
					logger.log(LogStatus.PASS, "TestCaseNo::"+TestCaseName+":: PASS"); 
					
					Assert.assertTrue(TestCaseNo+"--"+TestCaseName,true);
					
					
				}

				else {

					logger.log(LogStatus.FAIL, "TestCaseNo::"+TestCaseName+":: FAIL"); 
					Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);

				}

		}


		catch(Exception e){

			System.out.println("The exception is "+e);

		}


		finally{

			extent.endTest(logger);
			extent.flush();
			
			FileOutputStream out1;
		
				out1 = new FileOutputStream(TC_ScFolder+"/"+"MDMP_155_Stock_Update_Flow_RMS_To_BY_"+Final_Result+".docx");
				doc.write(out1);
				out1.close();
				doc.close();

		}



	}

}
