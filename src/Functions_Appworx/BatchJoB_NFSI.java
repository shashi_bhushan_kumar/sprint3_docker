package Functions_Appworx;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import Utilities.utilityFileWriteOP;

public class BatchJoB_NFSI {

	
	public static boolean  RunJob (String JobName,String TestCaseNo,String ResultPath) {
		
		Boolean  result=false;
		 try {

		       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
			
		      Process p=  Runtime.getRuntime().exec( "cscript C:/Appworx_Job_Run/Appworx_BatchParam_WS1.vbs"+" "+JobName+","+TestCaseNo+","+ResultPath );
		      
		      System.out.println("Waiting for UFT Script Execution to be completed ...");
		        try {
					p.waitFor();
					
					result=true;
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 }
		 
		 
		 catch (IOException ex) {

		    }
		 
		 
		 finally{
	    	   
	    	return result;   
	    	   
	       }
		
		
	}

	
public static boolean  RunJob (String link,String userName,String userPwd,String TestCaseNo,String ResultPath,String JobName) {
		
		Boolean  result=false;
		 try {

		       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
			
		      Process p=  Runtime.getRuntime().exec( "cscript C:/Appworx_Job_Run/Appworx_BatchParam_WS1.vbs"+" "+link+","+userName+","+userPwd+","+TestCaseNo+","+ResultPath+","+JobName);
		      
		      System.out.println("Waiting for UFT Script Execution to be completed ...");
		        try {
					p.waitFor();
					
					
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
		        String batchstatus=utilityFileWriteOP.ReadBatchResult("C:/Appworx_Job_Run/BatchStatus.txt");
		        System.out.println("BatchStatus "+batchstatus);
		        if(batchstatus.equalsIgnoreCase("Finished"))
		        {
		        	
		        	result=true;
		        }
		        else
		        {
		        	result=false;
		        }
		        
 }
		 
		 
		 catch (Exception ex) {
			 
			 System.out.println("Problem occurred during batch run...");
			 result=false;

		    }
		 
		 
		 finally{
	    	   
	    	return result;   
	    	   
	       }
		
		
	}


public static boolean  RunJob (String link,String userName,String userPwd,String TestCaseNo,String ResultPath,String JobName, String batchStatusFolder) {
	
	Boolean  result=false;
	 try {

	       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
		
	      Process p=  Runtime.getRuntime().exec( "cscript C:/UFT_SCRIPTS_AmazonWholesale/Appworx_BatchParam_WS1.vbs"+" "+link+","+userName+","+userPwd+","+TestCaseNo+","+ResultPath+","+JobName+","+batchStatusFolder);
	      
	      System.out.println("Waiting for UFT Script Execution to be completed ...");
	        try {
				p.waitFor();
				
				
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        String batchstatus=utilityFileWriteOP.ReadBatchResult(batchStatusFolder);
	        System.out.println("BatchStatus "+batchstatus);
	        if(batchstatus.equalsIgnoreCase("Finished"))
	        {
	        	
	        	result=true;
	        }
	        else
	        {
	        	result=false;
	        }
	        
}
	 
	 
	 catch (Exception ex) {
		 
		 System.out.println("Problem occurred during batch run...");
		 result=false;

	    }
	 
	 
	 finally{
    	   
    	return result;   
    	   
       }
	
	
}

public static boolean  JDA_Receiving_UFT() {
	
	Boolean  result=false;
	 try {

	       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
		
	      Process p=  Runtime.getRuntime().exec( "cscript C:/Users/systcsw1/Desktop/PO/JDA_Receiving.vbs");
	      
	      System.out.println("Waiting for UFT Script Execution to be completed ...");
	        try {
				p.waitFor();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        String batchstatus=utilityFileWriteOP.ReadBatchResult("C:/Users/systcsw1/Desktop/PO/JDA_Status.txt");
	        System.out.println("JDA Status "+batchstatus);
	        if(batchstatus.equalsIgnoreCase("Completed"))
	        {
	        	result=true;
	        }
	        else
	        {
	        	result=false;
	        }
	        
}
	 catch (Exception ex) {
		 
		 System.out.println("Problem occurred during receiving...");
		 result=false;

	    }
	 finally{
    	   
    	return result;   
    	   
       }
	
	
}



public static boolean  RunUFT_Script(String TestCaseNo,String ResultPath,String script_Location,String scriptStatus) {
	
	Boolean  result=false;
	 try {

	      // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );	
		 
		 System.out.println("Inside try");
		 System.out.println("Call parm 1:"+TestCaseNo);
		 System.out.println("Call parm 2:"+ResultPath);
		 System.out.println("script_Location:"+script_Location);
		 
	      Process p1=  Runtime.getRuntime().exec( "cscript "+script_Location+""+" "+TestCaseNo+","+ResultPath);
//	      Process p1=  Runtime.getRuntime().exec( "cscript C:/UFT_Scripts_Exec_Source/RPM_Price_Change/RPM_Price_ChangeVB.vbs"+" "+TestCaseNo+","+ResultPath);


	      System.out.println("Waiting for UFT Script Execution to be completed ...");
	        try {
				
	        	p1.waitFor();
				
				
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        
	        String scriptstatus=utilityFileWriteOP.ReadBatchResult(""+scriptStatus+"UFTScriptStatus.txt");
	        System.out.println("UFT Script Execution Status :"+scriptstatus);
	        if(scriptstatus.equalsIgnoreCase("PASS"))
	        {
	        	
	        	result=true;
	        }
	        else
	        {
	        	result=false;
	        }
	        
	   }
	 
	 
	 
	 
	 catch (Exception ex) {
		 
		 System.out.println("Problem occurred during batch run...");
		 result=false;

	   }		 
	 finally{
    	   
    	return result;   
    	   
       }				
}



public static String  UFTResultsPath(String scriptStatus) {
	
	String  result=null;
	 try {

	        String respath=utilityFileWriteOP.ReadBatchResult(""+scriptStatus+"UFTResPath.txt");
	        System.out.println("UFT Script Execution Screenshots result folder path :"+respath);
	        result=respath;
	        return result;
	        
	   }
	 
	 catch (Exception ex) {
		 
		 System.out.println("Problem occurred during UFT Script run...");
		 result=null;

	   }		 
	 finally{
    	   
    	return result;   
    	   
       }				
}


}
