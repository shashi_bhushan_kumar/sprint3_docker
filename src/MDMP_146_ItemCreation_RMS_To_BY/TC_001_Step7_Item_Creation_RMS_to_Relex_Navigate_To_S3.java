package MDMP_146_ItemCreation_RMS_To_BY;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;























import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Functions.McColls_Amazon_RackSpace;
import Functions.NFSI_PostCall;
import Utilities.*;



public class TC_001_Step7_Item_Creation_RMS_to_Relex_Navigate_To_S3 {

	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	public static XWPFRun xwpfRun=null;
	
	
	String URL;
	String DynamoURL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String Final_Result="";
	String Downloadfilepath="";
	String TestDataPath;	
	Integer stepnum=0;
	String downloadFilepath="";
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	String ChromeBrowserExePath=null;
	
	

	public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

		extent=extent1;
		logger=logger1;
		ResultPath=ResultPath1;
		TCFolder=TCFolder1;
		xwpfRun=xwpf1;
	}



	public static String getResultPath()
	{
		return ResultPath;

	}


	public static String getTCFolderPath()
	{
		return TCFolder;

	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		
		ChromeBrowserExePath=ProjectConfigurations.LoadProperties("ChromeBrowserExePath");
		BrowserPath=ProjectConfigurations.LoadProperties("BrowserPath");
				
		DriverPath=ProjectConfigurations.LoadProperties("DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("DriverName");
		DriverType=ProjectConfigurations.LoadProperties("DriverType");
		
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");



		TestDataPath=ProjectConfigurations.LoadProperties("TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("SheetName");
	
		ResultPath=getResultPath();

		
	}

	@After
	public void tearDown() throws Exception {
		
      driver.quit();
      
      //System.exit(1);
	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "Item_creation_Flow_from_RMS_to_BY";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		
		String s3username= null;
		String s3password = null;
		String bucketpath=null;
		String NasFileName=null;


		

		String consolidatedScreenshotpath="";
		//-------------------------------------------------


		int r = 0;
		try {


			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}


			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
				System.out.println("Keyword: "+Keyword);
				System.out.println("TestKeyword: "+TestKeyword);


				if(occurances>0){
					break;
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();			
					s3username=sheet1.getCell(map.get("S3UserName"), r).getContents().trim();
					s3password=sheet1.getCell(map.get("S3Password"), r).getContents().trim();
					bucketpath=sheet1.getCell(map.get("bucketpath"), r).getContents().trim();
					NasFileName=sheet1.getCell(map.get("NasFileName"), r).getContents().trim();
					
					
		            consolidatedScreenshotpath=getTCFolderPath()+"/";

					System.setProperty(DriverName,DriverPath);

					
					if((DriverType.equalsIgnoreCase("ChromeDriver"))&&(BrowserPath.equalsIgnoreCase("Default"))){
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", 0);
						chromePrefs.put("download.prompt_for_download", "false");
						chromePrefs.put("download.", "false");
						chromePrefs.put("download.default_directory", System.getProperty("user.dir")+Downloadfilepath);
						ChromeOptions options = new ChromeOptions();
					//	options.setBinary(ChromeBrowserExePath);

						options.setExperimentalOption("prefs", chromePrefs);
						options.setExperimentalOption("useAutomationExtension", false);
						DesiredCapabilities cap = DesiredCapabilities.chrome();
						cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						cap.setCapability(ChromeOptions.CAPABILITY, options);
						driver = new ChromeDriver(cap);
						wait=new WebDriverWait(driver, 60);			
						driver.manage().deleteAllCookies();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						
						wait = new WebDriverWait(driver, 30);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
					
						
						driver.manage().deleteAllCookies();
						driver.get("https://morrisonspreprod.signin.aws.amazon.com/console");

						driver.manage().window().maximize();
						
					}

					else {
						
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", 0);
						chromePrefs.put("download.prompt_for_download", "false");
						chromePrefs.put("download.", "false");
						chromePrefs.put("download.default_directory", System.getProperty("user.dir")+Downloadfilepath);
						ChromeOptions options = new ChromeOptions();
					    
						options.setBinary(ChromeBrowserExePath);  // setup chrome binary path

						options.setExperimentalOption("prefs", chromePrefs);
						options.setExperimentalOption("useAutomationExtension", false);
						DesiredCapabilities cap = DesiredCapabilities.chrome();
						cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						cap.setCapability(ChromeOptions.CAPABILITY, options);
											
						driver = new ChromeDriver(cap);
						wait=new WebDriverWait(driver, 60);			
						driver.manage().deleteAllCookies();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
					
						
						wait = new WebDriverWait(driver, 30);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	

						driver.manage().deleteAllCookies();
						driver.get("https://morrisonspreprod.signin.aws.amazon.com/console");

						driver.manage().window().maximize();   // Comment for lower version of chrome browser
						
						
						
					}
					
					
					boolean logins3 = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, s3username, s3password,ResultPath,xwpfRun, logger);
					if(logins3==true) {
						
						logger.log(LogStatus.PASS, "Logged in to Amazon Rackspace");
					}else{

						
						logger.log(LogStatus.FAIL, "Error occured in login to Amazon Rackspace");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					

					//*****************************************Navigate to S3**********************************
					boolean navigatetos3 = McColls_Amazon_RackSpace.navigateToS3(driver, wait, TestCaseNo,ResultPath,xwpfRun, logger);
					if(navigatetos3==true) {
						
						logger.log(LogStatus.PASS, "Navigated to S3 in Amazon Rackspace");
					}
					else{
						
						logger.log(LogStatus.FAIL, "Error occured in navigating through Amazon Rackspace");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					

					//***************************************Navigation to specific folder at S3**********************

					boolean validatefileAtS3 = McColls_Amazon_RackSpace.searchFileinBucketPath(bucketpath,NasFileName,driver, wait,TestCaseNo,ResultPath,xwpfRun, logger);
					if(validatefileAtS3==true) {
						
						logger.log(LogStatus.PASS, "File validated at S3 successfully");
					}
					else{
						
						logger.log(LogStatus.FAIL, "Error occured in navigating through Amazon Rackspace");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
				


					if(logins3 && navigatetos3 && validatefileAtS3){
					
				//	if(logins3 && navigatetos3){

						Final_Result="PASS"; 

						excelCellValueWrite.writeValueToCell(Final_Result, r , 3, DriverSheetPath, driversheetname);

						Assert.assertTrue(TestCaseNo+"--"+TestCaseName,true);
					}

				}

			} 
		}



		catch (Exception e) {

			e.printStackTrace();
			
			

			Final_Result="FAIL"; 

			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
		}
		finally
		{  
			
			

		}
	}

}

