package MNMP_168_Orders_RMS_To_BY;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.*;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import Utilities.GetTCDetailsFromDataSheet;
import Functions_Appworx.*;
import Utilities.FolderZipper;
import Utilities.MyException;
import Utilities.ProjectConfigurations;
import Utilities.Reporting_Utilities;
import Utilities.RowGenerator;
import Utilities.excelCellValueWrite;
import Utilities.getCurrentDate;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class TC_008_Step1_ExecuteJob_NB_RMSE_BLUEYONDER_ORDERS {

	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	 public static XWPFRun xwpfRun=null;

	static String  TestKeyword = "Orders_Flow_from_RMS_to_BY";	
	static String TestCaseNo=null;
	static String TestCaseName=null;
	
	String ScFolderPathUFT=null;

	String ResultPathUFT=null;
	Integer stepnum=0;
	String JobName="NB_RMSE_BLUEYONDER_ORDERS";

	String link="http://xeapx01x.unix.morrisons.net:5050/APXCPT01/Intro.html";
	String userName="SYSTCS9F";
	String userPwd="Changeme";


	public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

		extent=extent1;
		logger=logger1;
		ResultPath=ResultPath1;
		TCFolder=TCFolder1;
		xwpfRun=xwpf1;
	}


	public static String getResultPath()
	{
		return ResultPath;

	}


	public static String getTCFolderPath()
	{
		return TCFolder;

	}

	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {

		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");

		TestCaseNo=GetTCDetailsFromDataSheet.TCDetails(TestKeyword).get("TestCaseNo");
		TestCaseName=GetTCDetailsFromDataSheet.TCDetails(TestKeyword).get("TestCaseName");

	}
	//*******************************************************************************************************



	@After
	public void tearDown() throws Exception {


	}

	@Test
	public void test() throws IOException {

		ResultPathUFT=ProjectConfigurations.LoadProperties("ResultPathUFT");

		try{	

			System.out.println("Job name is "+JobName);			

			Boolean res=BatchJoB_NFSI.RunJob(link,userName,userPwd,TestCaseNo,ResultPathUFT,JobName);

			if(res){

				logger.log(LogStatus.PASS, "Execute Appworx job "+JobName+" : Successful");

				Assert.assertTrue("Execute Appworx job "+JobName,true);

			}			

			else{

				logger.log(LogStatus.FAIL, "Execute Appworx job " + JobName + ": UnSuccessful");

				Assert.assertTrue("Execute Appworx job "+JobName,false);

				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
		}



		catch (Exception e) {

			e.printStackTrace();
			Assert.assertTrue("Execute Appworx job "+JobName,false);

		}


		finally

		{  
			String dest=getTCFolderPath().replace("\\", "/");

			String  LatestScFile=FolderZipper.copySrcFolderToDestFolder(ResultPathUFT+"/Screenshot/"+TestCaseNo, getTCFolderPath());

			if(LatestScFile!=null){
				logger.log(LogStatus.INFO, "Evidance link for Appworx Job -"+JobName+" <a href='file:///"+dest+"/"+LatestScFile+"'>     EvidanceLink</a>");

			}
			


		}


	}}


